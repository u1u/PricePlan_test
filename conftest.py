# pylint: disable=redefined-outer-name
''' Pytest fixtures for testing interaction with PricePlan '''

import copy
import pytest
from test.pytest.price_plan_environment import Env
from test.pytest.utility import make_sample_stream_json
from test.pytest.examples import SAMPLE_PROJECT_DATA


@pytest.fixture(scope='session')
def fx_stream():

    ''' Makes stream object with test data '''

    return make_sample_stream_json(SAMPLE_PROJECT_DATA, 10)


@pytest.fixture(scope='session')
def fx_var_id_val(fx_stream):

    ''' Returns object that maps variable id with
        its value from test stream for each project'''

    tmp = {}
    res = {}

    def get_variable_value(dct, var_name):

        ''' Recursively walk over project object'''

        if hasattr(dct, 'keys'):
            for key, val in dct.items():
                get_variable_value(val, var_name+'_'+key)
        else:
            tmp[Env.var_name_to_id[var_name[1:]]] = tmp['last'] = dct

    for project_id, obj in fx_stream.items():
        get_variable_value(obj, "")
        res[project_id] = copy.deepcopy(tmp)

    return res


@pytest.yield_fixture(scope='session')
def fx_proj_subs_id(fx_stream):

    ''' Returns object that maps project id with
        related subscription id '''

    proj_subs_id = {}

    for index, key in enumerate(fx_stream.keys()):
        proj_subs_id[key] = Env.subscribes_list[index]

    yield proj_subs_id

    Env.destroy()
