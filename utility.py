# pylint: disable=no-member
'''some utility functions'''

import random
import copy
import string
import uuid


def get_variable_names(val):

    '''Recurcievely walks over json building names'''

    res = []

    def rec(obj, name):
        ''' recurcive call '''
        if hasattr(obj, 'keys'):
            for key, val in obj.items():
                rec(val, name + '_' + key)
        else:
            res.append(name[1:])
    rec(val, "")

    return res


def set_object_random_values(obj):

    '''Fills stream object with random values'''

    random.seed()
    for key, val in obj.items():
        if not hasattr(val, 'keys'):
            obj[key] = random.uniform(1000, 30000)
        else:
            set_object_random_values(val)


def make_sample_stream_json(obj, number):

    ''' Makes stream object with test data '''

    res = {}
    random.seed()
    project_ids = []
    num = randint(10000, 999999)

    for _ in range(number):
        while num in project_ids:
            num = randint(10000, 999999)
        project_ids.append(num)

    for proj_id in project_ids:
        project_data = copy.deepcopy(obj)
        set_object_random_values(project_data)
        res[proj_id] = project_data

    return res


def multiple_call(func):

    '''Decorator that runs a function multiple times'''

    def call(*args):
        '''decorating function'''

        count = 0
        while count < 100:
            count += 1
            if func(*args):
                return True
        return False
    return call


def randint(left, right):

    ''' generates random integer in range [left, right]'''

    return uuid.uuid1().int % (right - left) + left


def random_string():

    ''' generates a random string '''

    res = ''.join((random.choice(string.ascii_lowercase
                                 + string.digits)) for _ in range(5))
    return res
