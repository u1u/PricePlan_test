'''Test cases for interacting with PricePlan'''

from requests import get
from harvest.make_meters import PricePlanSetter, PricePlanException
from harvest.Settings import BILLING_URL, API_KEY, PASS
from test.pytest.price_plan_environment import Env


def test_set_meters_all_success(fx_proj_subs_id, fx_stream):

    ''' Checks, if PricePlan class doesn't raise exceptions'''

    pp_object = PricePlanSetter(fx_proj_subs_id)
    try:
        pp_object.set_meters(fx_stream)
    except PricePlanException:
        assert False
    assert True


def test_meter_last_values(fx_var_id_val, fx_proj_subs_id):

    ''' Checks if values of meters are set correctly '''

    subs_proj_id = {}
    for key, val in fx_proj_subs_id.items():
        subs_proj_id[val] = key

    for sub_id in Env.subscribes_list:
        values = []

        url = "{}subscribes/{}/meters/last".format(BILLING_URL, sub_id)
        req = get(url, auth=(API_KEY, PASS))

        assert req.status_code == 200 and req.json()['success']

        pp_var_value = req.json()['data'][0]['variable_id']
        project_id = subs_proj_id[sub_id]

        for key, val in fx_var_id_val[project_id].items():
            values.append(round(val, 5))

        assert round(float(pp_var_value), 5) in values
