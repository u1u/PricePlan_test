'''Constants for PricePlan environment'''

from test.pytest.utility import random_string


TEST_PRODUCT = {
    "name": random_string(),
    "description": "awesome description",
    "group": "",
    "cost": 48,
    "periodUnits": 1,
    "periodMeasure": "month",
    "tax": 0,
    "pre": 100,
    "status": 2,
    "values": {},
    "periods": [{"units": "1", "measure": "month", "discount": "0"}],
    "variables": {},
    "templates": {
        "invoice": "Подписка на {{product.name}} за пери"
                   "од c {{period.start}} по {{period.finish}}",
        "tax_receipt": "Подписка на {{product.name}} за пери"
                       "од c {{period.start}} по {{period.finish}}",
        "act": "Подписка на {{product.name}} за пери"
               "од c {{period.start}} по {{period.finish}}"
    }
}

TEST_CLIENT = {
    "values": {
        26: "710600347232",
        24: "300032, Тула г, Ломоносова ул., 38-5.",
        27: "",
        29: ""
    },
    "type": 2,
    "name": "Тестовый Фёдор Васильевич"
}

TEST_SUBSCRIPTION = {

    "client": 10,
    "doc_number": "",
    "period": 27,
    "product": 19,
    "values": {}
}

SAMPLE_PROJECT_DATA = {
    random_string(): {
        random_string(): 3453,
        random_string(): 2345,
        random_string(): 344,
        random_string(): 234,
        random_string(): 5765
    },
    random_string(): {
        random_string(): {
            random_string(): 345,
            random_string(): 765,
            random_string(): 345,
            random_string(): 285,
            random_string(): 9874,
            random_string(): {
                random_string(): 345,
                random_string(): 765,
                random_string(): 345,
                random_string(): 285,
                random_string(): 9874
            }
        },
        random_string(): {
            random_string(): 345,
            random_string(): 765,
            random_string(): 345,
            random_string(): 285,
            random_string(): 9874
        },
        random_string(): 46326,
        random_string(): 8654,
        random_string(): 4568,
        random_string(): 2568
    }
}

VARIABLE_DATA = {
    "is_searched": False,
    "is_private": False,
    "meta": {
        "priced": True,
        "meta": {
            "min": 0,
            "cost": 1,
            "tax": 0,
            "type": "fact",
            "rules": [],
            "meters": "max",
            "fact": 0
        }
    },
    "templates": {
        "invoice": "{{variable.name}} for period from {{pe"
                   "riod.start}} to {{period.finish}}",
        "tax_receipt": "{{variable.name}} for period fr"
                       "om {{period.start}} to {{period.finish}}",
        "act": "{{variable.name}} for period from {{peri"
               "od.start}} to {{period.finish}}"
    }
}
