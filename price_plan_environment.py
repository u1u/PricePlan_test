# pylint: disable=invalid-name
'''PricePlan test environment'''

import copy
from requests import get, post, delete
from harvest.make_meters import PricePlanException
from harvest.Settings import API_KEY, BILLING_URL, PASS
from test.pytest.utility import get_variable_names, multiple_call
from test.pytest.examples import (TEST_CLIENT,
                                  TEST_PRODUCT,
                                  TEST_SUBSCRIPTION,
                                  SAMPLE_PROJECT_DATA,
                                  VARIABLE_DATA)


class TestEnvironment(object):

    ''' Modules that manage objects in PricePlan '''

    def __init__(self):
        self.subscribes_list = []  # list of created subscribes ids
        self.test_product_id = None  # id of created test product
        self.test_client_id = None  # id of created test client
        self.var_name_to_id = {}  # maps test variable name to its id

        self.success_variables = []  # successfully created variables
        self.subs_num = 10  # number of test subscriptions

        if not self.create_all():
            self.destroy()
            raise PricePlanException

    def create_all(self):

        '''create all necessary for testing objects and
        and initialize class fields'''

        if self.create_variables():
            if self.create_product() and self.create_client():
                return self.create_subscriptions()
            else:
                return False
        else:
            return False

    def create_variables(self):

        ''' must return dict with variable_name->variable_id '''

        # init
        var_list = get_variable_names(SAMPLE_PROJECT_DATA)
        url = BILLING_URL + 'variables/'

        # create variables
        for variable in var_list:
            data = {
                'type': 'real',
                'name': variable,
                'sign': variable,
                'meta': 4
            }
            req = post(url, json=data, auth=(API_KEY, PASS))
            if req.status_code == 200 and req.json()['success']:
                self.success_variables.append(variable)

        # check if something was not created
        if len(self.success_variables) != len(var_list):
            print("Error while creating variables1")
            return False

        # get var ids
        req = get(url, auth=(API_KEY, PASS))
        if req.status_code == 200 and req.json()['success']:
            success = 0
            for var in req.json()['data']:
                if var['name'] in var_list:
                    success += 1
                    self.var_name_to_id[var['name']] = var['id']
            if success != len(self.success_variables):
                print("Error while creating variables2")
                return False

        else:
            print("Error while creating variables3")
            return False

        return True

    def create_client(self):

        ''' creates client and returns true if succeed '''

        url = BILLING_URL + 'clients/'
        req = post(url, json=TEST_CLIENT, auth=(API_KEY, PASS))
        if req.status_code == 200 and req.json()['success']:
            self.test_client_id = req.json()['data']['id']
            return True
        else:
            print("Something bad happened while creating client")
            return False

    def create_product(self):

        ''' Creates test product '''

        data = copy.deepcopy(TEST_PRODUCT)
        data['variables'] = {}

        for _, val in self.var_name_to_id.items():
            data['variables'][val] = copy.deepcopy(VARIABLE_DATA)

        url = BILLING_URL + 'products/'
        req = post(url, json=data, auth=(API_KEY, PASS))
        if req.status_code == 200 and req.json()['success']:
            req = get(url, auth=(API_KEY, PASS))
            if req.status_code == 200 and req.json()['success']:
                for product in req.json()['data']:
                    if product['name'] == TEST_PRODUCT['name']:
                        self.test_product_id = product['id']
                        return True
            else:
                print("Something bad happened while creating test product2")
                return False
        else:
            print("Something bad happened while creating test product1")
            return False

    def create_subscriptions(self):

        ''' Creates test subscriptions and if succeed writes its ids to a
        specific field and returns true. It is possible
        to subscribe the same user to the same product many times '''

        success = 0
        data = copy.deepcopy(TEST_SUBSCRIPTION)
        data['client'] = self.test_client_id
        data['product'] = self.test_product_id
        url = BILLING_URL + 'subscribes/'

        for _ in range(self.subs_num):
            req = post(url, json=data, auth=(API_KEY, PASS))
            if req.status_code == 200 and req.json()['success']:
                self.subscribes_list.append(req.json()['data']['id'])
                success += 1

        if success != self.subs_num:
            print("Something bad happened while creating test subscriptions1")
            return False
        else:
            return True

    @multiple_call
    def remove_subscriptions(self):

        '''Try to remove created subscriptions and return True if succeed'''

        if len(self.subscribes_list) == 0:
            return True

        for subs_id in self.subscribes_list:
            url = BILLING_URL + 'subscribes/' + str(subs_id)
            req = delete(url, auth=(API_KEY, PASS))
            if not (req.status_code == 200 and req.json()['success']):
                print("Error while removing subscriptions2")
                return False

        return True

    @multiple_call
    def remove_client(self):

        '''Try to remove created client and return True if succeed'''

        url = BILLING_URL + 'clients/' + str(self.test_client_id)
        req = delete(url, auth=(API_KEY, PASS))
        if req.status_code == 200 and req.json()['success']:
            return True
        else:
            print("Error while removing client")
            return False

    @multiple_call
    def remove_product(self):

        '''Try to remove created product and return True if succeed'''

        data = copy.deepcopy(TEST_PRODUCT)
        data['variables'] = {}

        url = BILLING_URL + 'products/' + str(self.test_product_id)
        req = post(url, json=data, auth=(API_KEY, PASS))
        if not(req.status_code == 200 and req.json()['success']):
            print("Error while removing product")
            return False

        req = delete(url, auth=(API_KEY, PASS))
        if req.status_code == 200 and req.json()['success']:
            return True
        else:
            print("Error while removing product")
            return False

    @multiple_call
    def remove_variables(self):

        ''' Try to remove created variables and return True if succeed.
        Removing variables is impossible, if they were initialized in
        PricePlan '''

        url = BILLING_URL + 'variables/'
        req = get(url, auth=(API_KEY, PASS))

        if req.status_code == 200:
            for var in req.json()['data']:

                if var['name'] in self.success_variables:
                    del_req = delete(url + str(var['id']),
                                     auth=(API_KEY, PASS))
                    if del_req.status_code == 200 and \
                            del_req.json()['success']:
                        self.success_variables.remove(var['name'])
                    else:
                        print("Error while removing variables1")
                        return False
        else:
            print("Error while removing variables2")
            return False

        return True

    def destroy(self):

        '''Call other removing methods'''

        print("\nDESTROYING!!!!")
        subs_removed = self.remove_subscriptions()
        client_removed = self.remove_client()
        product_removed = self.remove_product()
        # d = self.remove_variables()
        if not(subs_removed and client_removed and product_removed):
            raise PricePlanException


Env = TestEnvironment()
